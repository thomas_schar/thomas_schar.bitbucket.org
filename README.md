# Thomas Schar's BitBucket Page
(What are BitBucket [Pages?](http://pages.bitbucket.org))

This is really just an easy way to provide public read access to particular things I have in my other projects such as: particular wiki pages, release builds of apps I'm working on and so forth. It is still very much a work in progress right now.

## Current Useful Content:

* [Scharman No Bullshit Swift Overview](https://bitbucket.org/thomas_schar/thomas_schar.bitbucket.org/wiki/Scharman%20No%20Bullshit%20Swift%20Overview)
* Space Hulk Build Access
* MyBudget App Build Access
* Gyruss App Build Access